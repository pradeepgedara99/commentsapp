package com.example.paginationexampleapp.network

import com.example.paginationexampleapp.models.Comment
import java.io.IOException

class CommentRepository(private val commentService: CommentService) {
    suspend fun getComments(page: Int, limit: Int): List<Comment> {
        val response = commentService.getComments(page, limit)
        if (response.isSuccessful) {
            return response.body() ?: emptyList()
        } else {
            throw IOException("Error fetching comments")
        }
    }
}
