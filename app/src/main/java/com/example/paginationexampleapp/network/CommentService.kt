package com.example.paginationexampleapp.network

import com.example.paginationexampleapp.models.Comment
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface CommentService {
    @GET("comments")
    suspend fun getComments(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Response<List<Comment>>

    companion object {
        fun create(): CommentService {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(CommentService::class.java)
        }
    }
}


