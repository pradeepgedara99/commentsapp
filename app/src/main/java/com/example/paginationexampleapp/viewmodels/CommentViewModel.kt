package com.example.paginationexampleapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.paginationexampleapp.models.Comment
import com.example.paginationexampleapp.network.CommentService
import kotlinx.coroutines.launch

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf


class CommentViewModel : ViewModel() {
    private val originalComments = mutableListOf<Comment>()
    private var currentPage = 1
    private val pageSize = 10

    // Use MutableState to manage the list of comments
    private val _comments = mutableStateOf<List<Comment>>(emptyList())
    val comments: MutableState<List<Comment>> = _comments

    init {
        loadComments()
    }

    private fun loadComments() {
        viewModelScope.launch {
            try {
                val response = CommentService.create().getComments(currentPage, pageSize)
                if (response.isSuccessful) {
                    val newComments = response.body() ?: emptyList()
                    originalComments.addAll(newComments)
                    _comments.value = originalComments
                    currentPage++
                } else {
                    // Handle the error
                }
            } catch (e: Exception) {
                // Handle the error
            }
        }
    }

    fun searchComments(query: String) {
        val filteredComments = originalComments.filter { comment ->
            comment.name.contains(query, ignoreCase = true) ||
                    comment.email.contains(query, ignoreCase = true) ||
                    comment.body.contains(query, ignoreCase = true)
        }
        _comments.value = filteredComments
    }
}



