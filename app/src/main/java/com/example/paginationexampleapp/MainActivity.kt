package com.example.paginationexampleapp

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.paginationexampleapp.viewmodels.CommentViewModel
import com.example.paginationexampleapp.views.CommentList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val commentViewModel = ViewModelProvider(this)[CommentViewModel::class.java]
            CommentList(commentViewModel)
        }
    }

}




