package com.example.paginationexampleapp.views

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.paginationexampleapp.databinding.ItemCommentBinding
import com.example.paginationexampleapp.models.Comment

class CommentAdapter : RecyclerView.Adapter<CommentAdapter.CommentViewHolder>() {
    private val comments = mutableListOf<Comment>()

    fun setData(newData: List<Comment>) {
        comments.clear()
        comments.addAll(newData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCommentBinding.inflate(inflater, parent, false)
        return CommentViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bind(comments[position])
        Log.d("POSITION_ADAPTER", "onBindViewHolder: $position")
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    inner class CommentViewHolder(private val binding: ItemCommentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: Comment) {
            binding.textViewName.text = comment.name
            binding.textViewEmail.text = comment.email
            binding.textViewBody.text = comment.body
            binding.executePendingBindings()
        }
    }
}

