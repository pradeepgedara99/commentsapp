package com.example.paginationexampleapp.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.paginationexampleapp.models.Comment

@Composable
fun CommentItem(comment: Comment) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .background(Color.White)
            .padding(16.dp)
    ) {
        Text(
            text = comment.name,
            style = TextStyle(fontSize = 18.sp, fontWeight = FontWeight.Bold)
        )
        Text(
            text = comment.email,
            style = TextStyle(fontSize = 14.sp, fontStyle = FontStyle.Italic)
        )
        Text(
            text = comment.body,
            style = TextStyle(fontSize = 16.sp)
        )
    }
}